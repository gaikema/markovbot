<?php

namespace Api;

/**
	A class for an API, following the tutorial at
	http://coreymaynard.com/blog/creating-a-restful-api-with-php/
*/
abstract class Api {

	/**
		Property: method
		The HTTP method this request was made in,
		either GET, POST, PUT, or DELETE.
	*/
	protected $method;

	/**
		Property: endpoint
		The Model requested in the URI.
	*/
	protected $endpoint;

	/**
		Property: verb
		An optional additional descriptor about the endpoint,
		used for things that can not be handled by basic methods.
	*/
	protected $verb;

}