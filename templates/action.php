<?php

require __DIR__ . '/../vendor/autoload.php';
use App\Markov;

if(isset($_POST['inputText'])) {
	$text = trim($_POST['inputText']);

?>
<textarea class="form-control" rows="20" cols="60" readonly>
	<?php 
	// Check if input is empty (It crashes otherwise lol).
	if ($text !== '') {
		// http://stackoverflow.com/a/8614328/5415895
		$markov = new App\Markov($text);
		echo $markov->generate(); 
	}
	else 
		echo "Please enter more words.";
	?>
</textarea>
<img src="../image.php"></img>
<button class='btn btn-default' id='reset' onclick="resetButtonClicked()">Reset</button>
<br></br>
<?php
// This bothers me.
}
?>