<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("templates/header.php");
?>
<div class="container">
	<?php
	if(isset($_POST['Submit'])) {
		include("templates/action.php");
	}
	?>
	<form id="myForm" method="post" action="">
		<div class="form-group">
			<textarea class="form-control" rows="20" cols="60" name="inputText">
			</textarea>
		</div>
		<button type="submit" name="Submit" class="btn btn-default">Go!</button>
	</form>
</div><!--.container-->
<?php
include("templates/footer.php");
?>