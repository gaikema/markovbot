<?php
// Display a random image.

require_once __DIR__ . '/vendor/autoload.php';

use App;

header('Content-type: image/png');

$image = new App\RandomImage(256,256);
imagepng($image->generate());