<?php 

namespace App;

class Markov {
	
	// An associative array with all of the words and the words that follow them.
	private $chain;
	private $limit;

	/**
		Constructor.
		
		@param string $input The starting string.
	*/
	public function __construct($input, $limit=null) {
		$strings = str_word_count($input, 1);
		$words = array_flip($strings);
		//var_dump($words);
		//http://stackoverflow.com/a/2217200/5415895
		$words = array_fill_keys(array_keys($words), array());
		//var_dump($words);

		foreach ($strings as $index=>$string) {
			if ($index +1 < count($strings))
				array_push($words[$string],$strings[$index+1]);
		}
		$this->chain = $words;
		//var_dump($words);
		if ($limit==null)
			$this->limit = count($words);
		else 
			$this->limit = $limit;
	}

	/**
		Create the Markov chain.
	*/
	public function generate() {
		// https://github.com/TexAgg/MarkovTextGenerator/blob/master/Markov/Markov.cpp#L37
		$elem = key($this->chain);
		$output = $elem;
		$elem = $this->chain[$elem][array_rand($this->chain[$elem])];
		$output .= " " . $elem;

		$i = 0;
		while (count($this->chain[$elem]) != 0) {
			$elem = $this->chain[$elem][array_rand($this->chain[$elem])];
			$output .= " " . $elem;
			//var_dump($output);
			$i++;
			if (!array_key_exists($elem, $this->chain))
				break;
			if ($i == $this->limit)
				break;
		}
		return $output;
	}

	/**
		Getter method for the chain variable.

		@return An associative array.
	*/
	public function getChain() {
		return $this->chain;
	}
}