<?php
// http://stackoverflow.com/questions/9644097/how-to-generate-a-completely-random-image

namespace App;

class RandomImage {

	private $width;
	private $height;

	public function __construct($width=64, $height=64) {
		$this->width = $width;
		$this->height = $height;
	}

	public function generate() {
		$image = imagecreatetruecolor($this->width, $this->height);

		for ($row = 1; $row <= $this->height; $row++) {
			for ($col = 1; $col <= $this->width; $col++) {
				$red = mt_rand(0,255);
				$green = mt_rand(0,255);
				$blue = mt_rand(0,255);
				$color = imagecolorallocate($image, $red, $green, $blue);
				imagesetpixel($image, $col-1, $row-1, $color);
			}
		}
		return $image;
	}

}